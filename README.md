# STEPS TO INSTALL PHPMYADMIN (ALONG SIDE NGINX) ON SDC SERVERS (RHEL 8/Centos)

# PREREQUISITES NGINX INSTALLED AND SELINUX CONFIGURED
[Selinux config](https://stackoverflow.com/questions/22586166/why-does-nginx-return-a-403-even-though-all-permissions-are-set-properly)

## INSTALL PHP AND RELATED LIBRARIES

    sudo dnf -y install @php
    sudo dnf -y install php-zip php-json php-fpm
    sudo yum -y install php-mysqlnd

## MODIFY PHP-FPM USER AND GROUP FIELDS TO nginx user
    nano /etc/php-fpm.d/www.conf

### Edit the following fields 
    user = nginx
    group = nginx

## ENABLE PHP-FPM
    sudo systemctl enable --now php-fpm

## INSTALL PHPMYADMIN AND SET PERMISSION FOR NGINX USER (CHANGE VERSION ACCORDING TO PHP COMPATIBLITY AND PEFERENCE)
    wget https://www.phpmyadmin.net/downloads/phpMyAdmin-latest-all-languages.tar.gz
    tar xvf phpMyAdmin-latest-all-languages.tar.gz
    rm phpMyAdmin-latest-all-languages.tar.gz
    sudo mv phpMyAdmin-*/ /usr/share/phpmyadmin
    sudo mkdir -p /var/lib/phpmyadmin/tmp
    sudo chown -R nginx:nginx /var/lib/phpmyadmin
    sudo mkdir /etc/phpmyadmin/

## CREATE AND EDIT config.inc.php
    sudo cp /usr/share/phpmyadmin/config.sample.inc.php  /usr/share/phpmyadmin/config.inc.php
    sudo nano /usr/share/phpmyadmin/config.inc.php

### MAKE SURE TO SET blowfish_secret to a 32 character string (DONT USE THE SAME ONE WHICH IS HERE)
    $cfg['blowfish_secret'] = 'H2OmjGXxflSd8JwrwVlh6KW6s2rER63j'; 

### SETUP THE HOST IP IF YOU HAVE TO CONNECT TO A SEPARATE SERVER ELSE LEAVE IT TO LOCALHOST

## SET THE PERMISSION FOR PHP SESSION DIRECTORY (IMPORTANT)
    sudo chown -R nginx:nginx /var/lib/php/session/

## EDIT NGINX CONFIGURATION FOR ALLOWING PROXY THROUGH PHP-FPM
### SAMPLE CONFIGURATION TO HOST ON /phpmyadmin
    server {

	server_name  xyz.com;

	location / {
		proxy_pass http://localhost:9007;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
                proxy_pass_header Server;
	}
	
    location /phpmyadmin {
        root /usr/share/;
        index index.php index.html index.htm;
    
        location ~ ^/phpmyadmin/(.+\.php)$ {
            try_files $uri =404;
            root /usr/share/;
            fastcgi_pass unix:/run/php-fpm/www.sock;
            fastcgi_index index.php;
            fastcgi_intercept_errors on;
            fastcgi_buffers 8 16k;
            fastcgi_buffer_size 32k;
            fastcgi_connect_timeout 900;
            fastcgi_send_timeout 900;
            fastcgi_read_timeout 900;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include /etc/nginx/fastcgi_params;
        }

        location ~* ^/phpmyadmin/(.+\.(jpg|jpeg|gif|css|png|js|ico|html|xml|txt))$ {
            root /usr/share/;
        }
    }
    }

## RESTART NGINX AND PHP-FPM
    systemctl restart nginx
    systemctl restart php-fpm
